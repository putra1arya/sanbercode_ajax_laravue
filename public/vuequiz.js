
const newOb = new Vue({
    el : '#container',
    data : {
        countId : 0,
        nama : '',
        status: true,
        temp : '',
        index : '',
        users : []
    },
    methods :{
        tambahNama(){
            let input = this.nama.trim()//trim untuk menghindari inputan kosong yang berisi spasi saja
            if(input){
                // POST /someUrl
                this.$http.post('/api/store', {name: input}).then(response => {
                    this.users.unshift({
                        name: input
                    });
                });
            }


        },
        updateNama(){
            let input = this.nama.trim()
            this.$http.patch('/api/update/' + this.temp.id, {name: input}).then(response => {
                this.users.forEach((user,index) => {
                    if(index == this.index){
                        user.name = this.nama;
                    }
                });
                this.nama = '';
                this.status = true;
            });
        },
        editBtn(val,ind){
            this.temp = val;
            this.nama = val.name;
            this.status = false;
            this.index = ind;
        },
        hapusBtn(user, index){
            var jawab = window.confirm("Anda Yakin Menghapus Data ?")
            if(jawab){
                this.$http.delete('/api/delete/' + user.id).then(response => {
                    this.users.splice(index,1);
                });
            }
        }
    },
    mounted : function(){
        this.$http.get('/api/showall').then(response => {

            let result = response.body.data;
            this.users = result;
          });
    }
});
