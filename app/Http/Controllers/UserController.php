<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index(){
        $userName = User::orderBy('created_at', 'desc')->get();
        return UserResource::collection($userName);
    }
    public function store(Request $request){
        $user = User::create([
            'name' => $request['name']
        ]);
        return new UserResource($user);
    }
    public function delete($id){
        User::destroy($id);
        return 'success';
    }
    public function update(Request $request, $id){
        $user = User::find($id);
        $user->update([
            'name' => $request->name
        ]);
        return new UserResource($user);
    }

}
