<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quiz 2</title>
</head>
<body>
    <div id="container">
    <input type="text" v-model = "nama" >
    <button v-if="status" v-on:click="tambahNama()">Add</button>
    <button v-else="status" v-on:click="updateNama()" >Update</button>
    <ul v-for="(user,index) in users">
        <li>@{{user.name}} ||
            <button v-on:click="editBtn(user,index)">Edit </button> ||
            <button v-on:click="hapusBtn(user,index)">Hapus</button>
        </li>


    </ul>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script src="{{asset('vuequiz.js')}}"></script>
</body>
</html>
